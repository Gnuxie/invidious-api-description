(asdf:defsystem #:invidious-api-description
  :depends-on ("rest-api-description")
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Artistic 2.0"
  :serial t
  :components ((:file "description")))
