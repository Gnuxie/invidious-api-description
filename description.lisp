#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:invidious-api-description
  (:use #:cl)
  (:local-nicknames
   (#:rad #:rest-api-description))
  (:export
   #:videos
   #:search
   #:channels
   #:channel-videos
   #:channel-latest))

(in-package #:invidious-api-description)
;;; how to represent 'fields' key on all endpoints
;;; how to also represent 'language' support.

(rad:define-route api-v1 ("/api/v1/")
  ())

(rad:define-route videos
    (api-v1 "videos/" video-id)
  ((video-id :route)))

(rad:define-endpoint (:get videos))

(rad:define-route search (api-v1 "search")
    ((query :query
            :serial-name "q")
     (page :query)))

(rad:define-endpoint (:get search))

(rad:define-route channels (api-v1 "channels/" channel-id)
    ((channel-id :route)))

(rad:define-endpoint (:get channels))

(rad:define-route channel-videos (channels "/videos")
    ())

(rad:define-endpoint (:get channel-videos))

(rad:define-route channel-latest (channels "/latest")
    ())

(rad:define-endpoint (:get channel-latest))
